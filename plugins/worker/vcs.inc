<?php
/**
 * @file
 * Provide vcs worker setup task plugin.
 */

$plugin = array(
  'title' => t('vcs'),
  'description' => t('Provides vcs worker setup task execution.'),
  'perform' => 'worker_vcs_perform',
);

/**
 * Process version control repositories.
 *
 * @param $properties
 *   Associative array of job properties.
 * @return
 *   TRUE if successfull, otherwise FALSE.
 *
 * @see worker_perform() for a description of the properties array.
 */
function worker_vcs_perform(array $properties) {
  // Cycle through all repositories until complete or an error is encountered.
  foreach ($properties['vcs'] as $path => $url) {
    // Load plugin appropriate for handling the scheme.
    if ($vcs = worker_plugins_get_vcs($url)) {
      // Ensure the path specified exists.
      worker_directory_prepare(JOB_ROOT . '/' . $path);
      worker_chdir($path);

      // Checkout from the repository.
      worker_log('Checkout ' . $url . ' into /' . $path . '.');
      if (!$vcs['checkout']($url)) {
        worker_log('Failed to checkout ' . $url . '.');
        return FALSE;
      }

      // Return to the Drupal root after processing a repository successfully
      // to ensure that the next plugin is loaded properly.
      chdir(DRUPAL_ROOT);
    }
    else {
      worker_log('Unsupported scheme in ' . $url . '.');
      return FALSE;
    }
  }
  return TRUE;
}
