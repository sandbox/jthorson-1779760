<?php
/**
 * @file
 * Provide patch worker setup task plugin.
 */

$plugin = array(
  'title' => t('patch'),
  'description' => t('Provides patch worker setup task execution.'),
  'perform' => 'worker_patch_perform',
  'properties' => 'worker_patch_properties',
);

/**
 * List of properties applicable for this plugin
 */
function worker_patch_properties() {
  return array('patch', 'vcs', 'newline');
}

/**
 * Apply a list of patches.
 *
 * @param $properties
 *   Associative array of job properties.
 * @return
 *   TRUE if successfull, otherwise FALSE.
 *
 * @see worker_perform() for a description of the properties array.
 */
function worker_patch_perform(array $properties) {
  // Prepare patch storage directory.
  worker_directory_prepare($directory = DRUPAL_ROOT . '/' . file_directory_path() . '/patch');

  // Cycle through all the patches, download them and apply them.
  $delta = 0;
  foreach ($properties['patch'] as $url => $path) {
    if (!empty($properties['vcs'][$path]) && ($vcs = worker_plugins_get_vcs($properties['vcs'][$path]))) {
      // Use the path specified as the root the patch is to be applied to. Use
      // a delta to ensure patch file names do not conflict.
      worker_chdir($path);
      if (!($local = worker_fetch($url, $directory, $delta++))) {
        worker_log('Failed to fetch patch file from ' . $url);
        return FALSE;
      }
      $name = basename($url);

      // If requested check for non-unix newline characters.
      if ($properties['newline'] == 'unix' && strpos(file_get_contents($local), "\r") !== FALSE) {
        worker_log('Non-unix new line character(s) found in ' . $name . '.');
        return FALSE;
      }

      // Ensure that path the patch is to be applied to is readable.
      worker_log('Apply ' . $name . ' to /' . $path . '.');
      if (!$vcs['apply']($local)) {
        worker_log('Failed to apply patch ' . $name . '.');
        return FALSE;
      }
      worker_log_list('Modified files:', $vcs['modified files']($local));
    }
    else {
      worker_log('No repository corresponds to path /' . $path . ' for ' . basename($url) . '.');
      return FALSE;
    }
  }
  chdir(DRUPAL_ROOT);
  return TRUE;
}

/**
 * Fetch a file and place in directory.
 *
 * @param $url
 *   URL of file to fetch.
 * @param $directory
 *   Directory to place file in.
 * @param $suffix
 *   (Optional) Suffix to append to the local name of the file. (defaults to '')
 * @return
 *   Local path to file, or FALSE if retrieval/storage failed.
 */
function worker_fetch($url, $directory, $suffix = '') {
  // Override error handler so error_get_last() works.
  set_error_handler(function() { return false; });
  $contents = @file_get_contents($url);
  restore_error_handler();

  if ($contents === FALSE) {
    // Load error message and skip the function call portion.
    $error = error_get_last();
    worker_log('Unable to fetch ' . $url . ' due to [' . substr($error['message'], strpos($error['message'], ':') + 2) . '].');
    return FALSE;
  }

  // Save the file contents in the given directory.
  $local = $directory . '/' . basename($url) . $suffix;
  if (file_put_contents($local, $contents) === FALSE) {
    worker_log('Failed to save file data to ' . $local . '.');
    return FALSE;
  }
  return $local;
}

/**
 * Apply a patch in the working directory.
 *
 * @param $patch
 *   Path to patch file.
 * @return
 *   TRUE if successfull, otherwise FALSE.
 */
function worker_patch_apply($patch) {
  return worker_execute('patch -p0 -i ' . escapeshellarg($patch));
}

/**
 * Get a list of all files modified by a patch.
 *
 * @param
 *   Path to patch file, which must be in the unified format.
 * @return
 *   List of files modified by the patch.
 */
function worker_patch_modified_files($patch) {
  // Load the patch file and use regular expression to find all files modified
  // by the patch.
  $contents = file_get_contents($patch);
  preg_match_all('/^[\+-]{3} (.*?)\s/m', $contents, $matches, PREG_SET_ORDER);

  $files = array();
  foreach ($matches as $match) {
    // Ignore reference to /dev/null which will occur when a file is being
    // added or removed.
    if ($match[1] != '/dev/null') {
      $files[$match[1]] = $match[1];
    }
  }
  return array_values($files);
}
