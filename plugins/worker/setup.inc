<?php
/**
 * @file
 * Provide basic worker setup execute task plugin
 *
 */

$plugin = array(
  'title' => t('setup'),
  'description' => t('Basic worker setup tasks.'),
  'perform' => 'worker_setup_perform',
);

/**
 * Perform the job.
 *
 * @param $properties
 *   Associative array of properties defining the job.
 * @return
 *   An array containing a boolean for pass/fail and the result. The result is
 *   the result of the command that was executed.
 */
function worker_setup_perform(array $properties) {
  $properties['execute'] = $properties['setup'];
  unset($properties['setup']);
  return worker_execute_perform($properties);
}
