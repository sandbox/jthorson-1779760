<?php
/**
 * @file
 * Provide basic worker preparation task plugin
 *
 */

$plugin = array(
  'title' => t('Prepare'),
  'description' => t('Basic worker preparation tasks.'),
  'perform' => 'worker_prepare_perform',
);

/**
 * Perform preparation needed before job runs.
 *
 * @param $properties
 *   Associative array of job properties.
 * @return
 *   TRUE if successfull, otherwise FALSE.
 *
 * @see worker_perform() for a description of the properties array.
 */
function worker_prepare_perform(array $properties) {
  if (!worker_directory_prepare()) {
    return FALSE;
  }
  return array(TRUE, t("Job environment initialized"));
}
