<?php

/**
 * @file
 * Provide basic worker post-build execute task plugin
 *
 */

$plugin = array(
  'title' => t('build'),
  'description' => t('Basic worker setup tasks.'),
  'perform' => 'worker_build_perform',
);

/**
 * Perform the job.
 *
 * @param $properties
 *   Associative array of properties defining the job.
 * @return
 *   An array containing a boolean for pass/fail and the result. The result is
 *   the result of the command that was executed.
 */
function worker_build_perform(array $properties) {
  $properties['execute'] = $properties['build'];
  unset($properties['build']);
  return worker_execute_perform($properties);
}
