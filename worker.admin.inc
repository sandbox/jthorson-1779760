<?php

/**
 * @file
 * This file contains the administration functions for the 'Worker' module
 */

/**
 * The administration for for managing 'Worker' settings
 */
function worker_admin_config_form($form, &$form_state) {
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
    '#weight' => -10
  );
  $form['general']['worker_server'] = array(
    '#type' => 'textfield',
    '#title' => t('Server URL'),
    '#description' => t('The URL of the server to retrive jobs from, include ending forward slash.'),
    '#default_value' => variable_get('worker_server', ''),
    '#required' => TRUE,
  );
  $form['general']['worker_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Worker username'),
    '#description' => t('The username used by this worker when communicating with the job dispatcher.'),
    '#default_value' => variable_get('worker_username', 'worker'),
    '#required' => TRUE,
  );
  $form['general']['worker_password'] = array(
    '#type' => 'password_confirm',
    '#title' => t('Worker password'),
    '#description' => t('The password used by this worker when communicating with the job dispatcher.'),
    '#default_value' => variable_get('worker_password', 'worker'),
    '#required' => TRUE,
  );
  $form['general']['worker_queues'] = array(
    '#type' => 'textfield',
    '#title' => t('Allowed job queues'),
    '#default_value' => variable_get('worker_queues', 'worker'),
    '#description' => t('Comma separated list of qa.d.o queues from which this worker should attempt to retrieve jobs.'),
    '#maxlength' => 255,
    '#required' => TRUE,
  );
  $form['general']['worker_concurrency'] = array(
    '#type' => 'select',
    '#title' => t('Worker concurrency'),
    '#options' => range(1,16),
    '#default_value' => variable_get('worker_concurrency', 2),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}

/**
 * Validate worker admin configuration settings
 */
function worker_admin_config_form_validate($form, &$form_state) {
  if (!$form_state['values']['worker_server'] || !@parse_url($form_state['values']['worker_server'])) {
    form_set_error('worker_server', t('Server URL must be a complete URL, schema included.'));
  }
  else if ($form_state['values']['worker_server'][strlen($form_state['values']['worker_server'])-1] != '/') {
    form_set_error('worker_server', t('Server URL must end with a slash.'));
  }
}

